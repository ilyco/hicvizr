# hicvizR

Visualisation tool for Hi-C matrices (plotted triangularly) that can be matched with genomic regions of interest (e.g. genes and enhancers), as well as ChIP-seq signal tracks, TADs (as triangles) and A/B compartments. 

R version 3.4.4, Ubuntu 16.04.3 LTS, Typical run time <1 min, except when loading large data sets.

Pre-requisites (other R packages):

```
library(RColorBrewer)
library(ggplot2)
library(viridis)
library(rtracklayer)
library(GenomicRanges)
library(data.table)
library(ggthemes)
library(ggforce)
library(scales)
library(grid)
library(gridExtra)
library(GenomicInteractions)
library(cowplot)
```



Instructions for plotting chr14 at 300kb resolution (example data):

```
res = 300000
bins = import("testdata/chr14_bins_300000.bed")
hicobjfile = "testdata/THP1_Ctrl_300000_chr14.txt"
hicobj = fread(hicobjfile,skip=0,col.names=c("BinStart", "BinEnd","Value"))
hicobj = hicobj[hicobj$BinStart >= hicobj$BinEnd,]
maxvalue = max(hicobj$Value)

randomregions = import("testdata/chr14_randomregions.bed")
TADs = import("testdata/TADs_chr14.bed")

plot_hic(res,hicobjfull = hicobj,bins = bins,zoomlevel = 0,
         colourscheme = "fixed_symmetric_BluetoRed",
         fixedvalue = maxvalue, transformation = "None",
         location = "chr14:1-107349540",
         bed_list = list(randomregions),show_labels =FALSE,
         colours_bed ="darkred", bed_track_height = 0.5,
         TADs = TADs, TADs_colour="grey",
         TADs_track_height = 1,  
         outtype = "pdf",outfile_prefix = "TestPlot")
```
	  
![](testplot.png)
	  
	  